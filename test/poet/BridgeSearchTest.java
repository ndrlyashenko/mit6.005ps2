/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package poet;

import static org.junit.Assert.*;

import org.junit.Test;

import graph.ConcreteEdgesGraph;
import graph.Graph;

/**
 * Tests for BridgeSearch.
 */
public class BridgeSearchTest {
    
    // Testing strategy
    //   graph: null, valid implementation
    //   source, target: null; length: 0, >1
    //   paths source->target in graph: 0, 1, >1
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    @Test
    public void testBridgeSearchNullGraph(){
        final String bridge = BridgeSearch.search(null, "source", "target");
        assertEquals("", bridge);
    } 
    @Test
    public void testSearchNoPath(){
        final String bridge = BridgeSearch.search(new ConcreteEdgesGraph(), "source", "target");
        assertEquals("", bridge);
    }
    @Test
    public void testSearchAmongOnePath(){
        final Graph<String> graph = new ConcreteEdgesGraph();
        graph.set("source", "bridge", 1);
        graph.set("bridge", "target", 1);
        final String bridge = BridgeSearch.search(graph, "source", "target");   
        assertEquals("bridge", bridge);     
    }
}
