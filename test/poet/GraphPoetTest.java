/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package poet;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests for GraphPoet.
 */
public class GraphPoetTest {
    
    // Testing strategy
    //   corpus: null, wrong path, empty file
    //   input: null, length: 0, >1
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    // TODO tests
    @Test
    public void testNullCorpus(){
        //new GraphPoet(null);
    }
    @Test
    public void testNotExistingCorpus(){
        //new GraphPoet("empty");
    }
    @Test
    public void testEmptyCorpus(){
        //new GraphPoet(new File());
    }
    @Test
    public void testPoemEmptyInput(){
        //new GraphPoet(new File()).poem("Test");
    }
    @Test
    public void testPoem(){
        //new GraphPoet(new File());
    }
}
