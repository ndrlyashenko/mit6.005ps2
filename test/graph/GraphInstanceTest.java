/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Test;

/**
 * Tests for instance methods of Graph.
 * 
 * <p>PS2 instructions: you MUST NOT add constructors, fields, or non-@Test
 * methods to this class, or change the spec of {@link #emptyInstance()}.
 * Your tests MUST only obtain Graph instances by calling emptyInstance().
 * Your tests MUST NOT refer to specific concrete implementations.
 */
public abstract class GraphInstanceTest {
    
    // Testing strategy
    //   weight: -1, 0, > 1
    //   source.length: 0, > 1
    //   target.length: 0, > 1
    
    /**
     * Overridden by implementation-specific test classes.
     * 
     * @return a new empty graph of the particular implementation being tested
     */
    public abstract Graph<String> emptyInstance();
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    @Test
    public void testInitialVerticesEmpty() {
        // TODO you may use, change, or remove this test
        assertEquals("expected new graph to have no vertices",
                Collections.emptySet(), emptyInstance().vertices());
    }
     
    @Test public void testAddVertex(){
        final Graph<String> graph = emptyInstance();

        assertTrue(graph.add("u1"));
        assertFalse(graph.add("u1"));
    }
    @Test public void testAddEdge(){
        final Graph<String> graph = emptyInstance();

        final int set1 = graph.set("u1", "v1", 1);
        assertEquals(0, set1);
    }
    @Test public void testChangeEdge(){
        final Graph<String> graph = emptyInstance();

        final int set1 = graph.set("u1", "v1", 1);
        final int set2 = graph.set("u1", "v1", 2);
        assertEquals(0, set1);
        assertEquals(1, set2);
    }
    @Test public void testRemoveExistingEdge(){
        final Graph<String> graph = emptyInstance();

        final int set1 = graph.set("u1", "v1", 1);
        final int set2 = graph.set("u1", "v1", 0);
        assertEquals(0, set1);
        assertEquals(1, set2);
    }
    @Test public void testRemoveNotExistingEdge(){
        final Graph<String> graph = emptyInstance();

        graph.set("u1", "v1", 0);
    }
    @Test public void testVertices(){
        final Graph<String> graph = emptyInstance();

        graph.vertices();
    }
    @Test public void testSources(){
        final Graph<String> graph = emptyInstance();

        graph.set("u1", "v1", 0);
        graph.sources("v1");

    }
    @Test public void testTargets(){
        final Graph<String> graph = emptyInstance();

        graph.set("u1", "v1", 0);
        graph.targets("u1");
    }
}
