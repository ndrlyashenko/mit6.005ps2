/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests for ConcreteEdgesGraph.
 * 
 * This class runs the GraphInstanceTest tests against ConcreteEdgesGraph, as
 * well as tests for that particular implementation.
 * 
 * Tests against the Graph spec should be in GraphInstanceTest.
 */
public class ConcreteEdgesGraphTest extends GraphInstanceTest {
    
    /*
     * Provide a ConcreteEdgesGraph for tests in GraphInstanceTest.
     */
    @Override public Graph<String> emptyInstance() {
        return new ConcreteEdgesGraph();
    }
    
    /*
     * Testing Edge...
     */    
    // Testing strategy for Edge
    //   source.length: 0, > 1
    //   target.length: 0, > 1
    //   weight: -1, 0, > 1
    //   source, target: null
    //   
    //   source may be equal to target
    
    @Test(expected = AssertionError.class)
    public void testEdgeEmptySource(){
        final Edge edge1 = new Edge(null, "target", 1);
        final Edge edge2 = new Edge("", "target", 1);
    }
    @Test(expected = AssertionError.class)
    public void testEdgeEmptyTarget(){
        final Edge edge1 = new Edge("source", null, 1);
        final Edge edge2 = new Edge("source", "", 1);
    }
    @Test(expected = AssertionError.class)
    public void testEdgeNegativeWeight(){
        final Edge edge1 = new Edge("source", "target", -1);        
    }
    @Test(expected = AssertionError.class)
    public void testEdgeZeroWeight(){
        final Edge edge1 = new Edge("source", "target", 0);           
    }
    @Test
    public void testGetSource(){
        final Edge edge1 = new Edge("source", "target", 1);   
        assert(edge1 != null);
        assertEquals("source", edge1.getSource());
    }
    @Test
    public void testGetTarget(){
        final Edge edge1 = new Edge("source", "target", 1);   
        assert(edge1 != null);
        assertEquals("target", edge1.getTarget());
    }
    @Test
    public void testGetWeight(){
        final Edge edge1 = new Edge("source", "target", 1);   
        assert(edge1 != null);
        assertEquals(1, edge1.getWeight());
    }
    @Test
    public void testToString(){
        final Edge edge1 = new Edge("source", "target", 1);   
        assert(edge1 != null);
        assertEquals("Edge: source->target, weight: 1", edge1.toString());
    }
    @Test
    public void testSelfPath(){
        final Edge edge1 = new Edge("source", "source", 1);   
        assert(edge1 != null);
    }
    
}
