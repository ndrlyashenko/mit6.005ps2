/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * An implementation of Graph.
 * 
 * <p>PS2 instructions: you MUST use the provided rep.
 */
public class ConcreteEdgesGraph implements Graph<String> {
    
    private final Set<String> vertices = new HashSet<>();
    private final List<Edge> edges = new ArrayList<>(); 

    // Abstraction function:
    //   vertices represent set of vertices (labels) of graph
    //   edges represent list of edges (connected pair of vertices with weight) of graph
    // Representation invariant:
    //   vertex may not be null or empty string
    //   edges may not contain duplicates
    // Safety from rep exposure:
    //   vertices, edges are private and immutable.

    @Override public boolean add(String vertex) {
        if(vertices.contains(vertex)){
            return false;
        }
        vertices.add(vertex);
        return true;
    }    

    @Override public int set(String source, String target, int weight) {
        if(weight < 0){
            return 0;
        }
        final List<Integer> foundWeight = edges.stream()
	        	.filter(e -> e.getSource().equals(source) && e.getTarget().equals(target))
	        	.map(e -> e.getWeight())
	        	.collect(Collectors.toList());
        if(weight == 0 || !foundWeight.isEmpty()){
	        edges.removeIf(e -> e.getSource().equals(source) && e.getTarget().equals(target));
        }
        if(weight > 0){
        	edges.add(new Edge(source, target, weight)); 
        }
        return foundWeight.isEmpty() ? 0 : foundWeight.get(0);     
    }
    
    @Override public boolean remove(String vertex) {
        if(!vertices.contains(vertex)){
            return false;
        }
        vertices.remove(vertex);  
       	edges.removeIf(e -> e.getSource().equals(vertex) || e.getTarget().equals(vertex));       
        return true;
    }
    
    @Override public Set<String> vertices() {
        return this.vertices;
    }
    
    @Override public Map<String, Integer> sources(String target) {
        return edges.stream()
                .filter(e -> e.getTarget().equals(target))
                .collect(Collectors.toMap(Edge::getSource, Edge::getWeight));
    }
    
    @Override public Map<String, Integer> targets(String source) {
        return edges.stream()
                .filter(e -> e.getSource().equals(source))
                .collect(Collectors.toMap(Edge::getTarget, Edge::getWeight));
    }
    
    public String toString(){
        return "Graph with vertices: " + this.vertices + ", edges: " + this.edges;
    }
    
}

/**
 * Edge represents edge on directed weighted graph.
 * Edge contains pair of vertices (target -> source) and weight.
 * Vertex is represented by String label.
 * Vertex must be non-empty string.
 * Source may be equal to target.
 * Weight must be positive.
 * Immutable.
 * This class is internal to the rep of ConcreteEdgesGraph.
 * 
 * <p>PS2 instructions: the specification and implementation of this class is
 * up to you.
 */
class Edge {
    
    private final String source;
    private final String target;
    private final int weight;

    // Abstraction function:
    //   represent connected pair of vertices (source -> target) on directed weighted graph.
    // Representation invariant:
    //   source.length > 0
    //   target.length > 0
    //   weight > 0
    // Safety from rep exposure:
    //   source, target, weight are private and immutable.
    
    public Edge(String source, String target, int weight){
        this.source = source;
        this.target = target;
        this.weight = weight;
        checkRep();
    }

    private void checkRep(){
        assert(this.source != null);
        assert(this.source.length() > 0);
        assert(this.target != null);
        assert(this.target.length() > 0);
        assert(this.weight > 0);
    }

    public String getSource(){
        return this.source;
    }

    public String getTarget(){
        return this.target;
    }

    public int getWeight(){
        return this.weight;
    }

    public String toString(){
        return "Edge: " + source + "->" + target + ", weight: " + weight;
    }
    
}
