/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package poet;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import graph.ConcreteEdgesGraph;
import graph.Graph;

/**
 * A graph-based poetry generator.
 * 
 * <p>GraphPoet is initialized with a corpus of text, which it uses to derive a
 * word affinity graph.
 * Vertices in the graph are words. Words are defined as non-empty
 * case-insensitive strings of non-space non-newline characters. They are
 * delimited in the corpus by spaces, newlines, or the ends of the file.
 * Edges in the graph count adjacencies: the number of times "w1" is followed by
 * "w2" in the corpus is the weight of the edge from w1 to w2.
 * 
 * <p>For example, given this corpus:
 * <pre>    Hello, HELLO, hello, goodbye!    </pre>
 * <p>the graph would contain two edges:
 * <ul><li> ("hello,") -> ("hello,")   with weight 2
 *     <li> ("hello,") -> ("goodbye!") with weight 1 </ul>
 * <p>where the vertices represent case-insensitive {@code "hello,"} and
 * {@code "goodbye!"}.
 * 
 * <p>Given an input string, GraphPoet generates a poem by attempting to
 * insert a bridge word between every adjacent pair of words in the input.
 * The bridge word between input words "w1" and "w2" will be some "b" such that
 * w1 -> b -> w2 is a two-edge-long path with maximum-weight weight among all
 * the two-edge-long paths from w1 to w2 in the affinity graph.
 * If there are no such paths, no bridge word is inserted.
 * In the output poem, input words retain their original case, while bridge
 * words are lower case. The whitespace between every word in the poem is a
 * single space.
 * 
 * <p>For example, given this corpus:
 * <pre>    This is a test of the Mugar Omni Theater sound system.    </pre>
 * <p>on this input:
 * <pre>    Test the system.    </pre>
 * <p>the output poem would be:
 * <pre>    Test of the system.    </pre>
 * 
 * <p>PS2 instructions: this is a required ADT class, and you MUST NOT weaken
 * the required specifications. However, you MAY strengthen the specifications
 * and you MAY add additional methods.
 * You MUST use Graph in your rep, but otherwise the implementation of this
 * class is up to you.
 */
public class GraphPoet {
    
    private final Graph<String> graph = new ConcreteEdgesGraph();
    
    // Abstraction function:
    //  represents graph
    // Representation invariant:
    //  graph may not be null, may not contain empty vertices, may not contain same edges
    // Safety from rep exposure:
    //  graph is private and immutable
    
    /**
     * Create a new poet with the graph from corpus (as described above).
     * 
     * @param corpus text file from which to derive the poet's affinity graph
     * @throws IOException if the corpus file cannot be found or read
     */
    public GraphPoet(File corpus) throws IOException {
        if(corpus == null){
            return;
        }
        final List<String> lines = Files.lines(Paths.get(corpus.getAbsolutePath()), Charset.forName("Cp1252"))
        		.collect(Collectors.toList());
        
        final List<String> words = Arrays.asList(String.join(" ", lines).split(" ")).stream()
        		.map(word -> word.toLowerCase())
        		.filter(word -> !word.isEmpty())
                .collect(Collectors.toList());    
        
        /**
         * @TODO
         * functional
         */
        int weight = 0;
        for(int i = 1; i < words.size(); ++i){  
            if(!words.get(i).equals(words.get(i-1))){
                if(weight > 0){
                    graph.set(words.get(i-1), words.get(i-1), weight);
                }
                graph.set(words.get(i-1), words.get(i), 1);
                weight = 0;
            }else{
                ++weight;
            }
        }
    }

    /**
     * Generate a poem.
     * 
     * @param input string from which to create the poem
     * @return poem (as described above)
     */
    public String poem(String input) {

        final List<String> words = Arrays.asList(input.split(" "));
        final List<String> poem = new ArrayList<>();
        for(int i = 1; i < words.size(); ++i){
        	final String source = words.get(i-1).toLowerCase();
        	final String target = words.get(i).toLowerCase();
        	System.out.println("source: " + source);
        	System.out.println("target: " + target);
        	
            final String bridge = BridgeSearch.search(graph, source, target);
            System.out.println("found bridge: " + bridge);
            poem.add(words.get(i-1));
            poem.add(bridge);
        }
        poem.add(words.get(words.size() - 1));
        return String.join(" ", poem);
    }

    /**
     * @TODO toString()
     * @return graph in human-read format
     */    
    public String toString(){
        return "Graph : " + graph;
    }
}
