/**
 * BridgeSearch provides method to search bridge word in graph.
 */
package poet;

import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import graph.Graph;

public class BridgeSearch{

    /**
     * Find bridge word, 
     * such as 2-edge path {@code source}->bridge->{@code target}, from {@code graph}.
     * Return empty string, if there is no such path in {@code graph}.
     *
     * @param  graph  graph
     * @param  source vertex label
     * @param  target vertex label
     * @return return bridge word or empty string
     */
    public static String search(Graph<String> graph, String source, String target){
        if(graph == null || source == null || target == null){
            return "";
        }
        /**
         * @TODO
         * functional
         */
    	String bridge = "";
        for(Entry<String, Integer> adj : graph.targets(source).entrySet()){
        	for(Entry<String, Integer> in : graph.targets(adj.getKey()).entrySet()){
        		if(in.getKey().equals(target)){
        			bridge = adj.getKey();
        		}
        	}
        }
        return bridge;
    }
 }